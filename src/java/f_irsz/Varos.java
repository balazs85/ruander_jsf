/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package f_irsz;

/**
 *
 * @author nbalazs
 */
public class Varos {
    private int megyeAzon;
    private String nev;
    private String irsz;

    public Varos(int megyeAzon, String nev, String irsz) {
        this.megyeAzon = megyeAzon;
        this.nev = nev;
        this.irsz = irsz;
    }

    public int getMegyeAzon() {
        return megyeAzon;
    }

    public void setMegyeAzon(int megyeAzon) {
        this.megyeAzon = megyeAzon;
    }

    public String getNev() {
        return nev;
    }

    public void setNev(String nev) {
        this.nev = nev;
    }

    public String getIrsz() {
        return irsz;
    }

    public void setIrsz(String irsz) {
        this.irsz = irsz;
    }
}
