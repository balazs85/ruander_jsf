/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package f_irsz;

/**
 *
 * @author nbalazs
 */
public class Megye {
    private int azon;
    private String nev;

    public Megye(int azon, String nev) {
        this.azon = azon;
        this.nev = nev;
    }
    
    public int getAzon() {
        return azon;
    }

    public void setAzon(int azon) {
        this.azon = azon;
    }

    public String getNev() {
        return nev;
    }

    public void setNev(String nev) {
        this.nev = nev;
    }
    
    
}
