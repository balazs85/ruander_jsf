/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package f_irsz;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author nbalazs
 */
@ManagedBean
@SessionScoped
public class IrszBean implements Serializable {

    private int megyeAzon;
    private String varosIrsz;
    private boolean varosListaTiltva = false;
    private IrszAdatok adatok = new IrszAdatok();

    public boolean isVarosListaTiltva() {
        return varosListaTiltva;
    }

    public int getMegyeAzon() {
        return megyeAzon;
    }

    public void setMegyeAzon(int megyeAzon) {
        this.megyeAzon = megyeAzon;
        varosIrsz = "";
        varosListaTiltva = megyeAzon == 0;
    }

    public String getVarosIrsz() {
        return varosIrsz;
    }

    public void setVarosIrsz(String varosIrsz) {
        this.varosIrsz = varosIrsz;
    }

    public List<Megye> getMegyek() {
        return (adatok.getMegyek());
    }

    public List<Varos> getVarosok() {
        return (adatok.getVarosok(megyeAzon));
    }
}
