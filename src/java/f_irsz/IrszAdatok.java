/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package f_irsz;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nbalazs
 */
public class IrszAdatok {

    private List<Megye> megyek;
    private List<Varos> varosok;

    public IrszAdatok() {
        megyek = new ArrayList<>();
        megyek.add(new Megye(1, "Pest"));
        megyek.add(new Megye(2, "Veszprém"));
        megyek.add(new Megye(3, "Somogy"));
        
        varosok = new ArrayList<>();
        varosok.add(new Varos(1, "Dabas", "2370"));
        varosok.add(new Varos(1, "Cegléd", "2700"));
        varosok.add(new Varos(1, "Fót", "2151"));
        varosok.add(new Varos(2, "Ajka", "8400"));
        varosok.add(new Varos(2, "Pápa", "8500"));
        varosok.add(new Varos(2, "Tapolca", "8300"));
        varosok.add(new Varos(2, "Devecser", "8460"));
        varosok.add(new Varos(3, "Balatonboglár", "8630"));
        varosok.add(new Varos(3, "Kaposvár", "7400"));
        varosok.add(new Varos(3, "Csurgó", "8840"));
        
    }
    
    
    public List<Megye> getMegyek() {
        return megyek;
    }

    public List<Varos> getVarosok() {
        return varosok;
    }
    
    public List<Varos> getVarosok(int megyeAzon) {
        List<Varos> eredmeny = new ArrayList<>();
        for (Varos v : varosok) {
            if (v.getMegyeAzon() == megyeAzon){
                eredmeny.add(v);
            }
        }
        return eredmeny;
    }   
    
}
