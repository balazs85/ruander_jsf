/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package c_bank;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nbalazs
 */
public class BankDAOArrayList implements BankDAOInterface{
    
    List<BankUgyfel> ugyfelek;

    public BankDAOArrayList() {
        ugyfelek = new ArrayList<>();
        ugyfelek.add(new BankUgyfel("1", "Béla", 1000000));
        ugyfelek.add(new BankUgyfel("2", "András", 100));
        ugyfelek.add(new BankUgyfel("3", "Lujza", -1000));
    }
        
    @Override
    public BankUgyfel ugyfelKeres(String azon) {
        for (int i = 0; i < ugyfelek.size(); i++) {
            if (ugyfelek.get(i).getAzon().equals(azon)){
                return ugyfelek.get(i);
            }
        }
        return null;
    }
    
}
