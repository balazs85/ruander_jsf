/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package c_bank;

import java.io.Serializable;

/**
 *
 * @author oktato2
 */
public class BankUgyfel implements Serializable{
    private String azon;
    private String nev;
    private Integer egyenleg;

    public BankUgyfel(String azon, String nev, Integer egyenleg) {
        this.azon = azon;
        this.nev = nev;
        this.egyenleg = egyenleg;
    }

    public String getAzon() {
        return azon;
    }

    public String getNev() {
        return nev;
    }

    public Integer getEgyenleg() {
        return egyenleg;
    }
    
    
}
