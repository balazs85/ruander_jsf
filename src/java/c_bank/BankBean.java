/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package c_bank;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class BankBean {

    private static BankDAOInterface adatbazis = new BankDAOArrayList();
    private String ugyfelAzon;
    private BankUgyfel ugyfel;

    public String getUgyfelAzon() {
        return ugyfelAzon;
    }

    public void setUgyfelAzon(String ugyfelAzon) {
        this.ugyfelAzon = ugyfelAzon.trim();
    }

    public String egyenlegMutat() {
        ugyfel = adatbazis.ugyfelKeres(ugyfelAzon);
        if (ugyfel == null) {
            return "nincs";
        } else if (ugyfel.getEgyenleg() < 0) {
            return "fenyeget";
        } else {
            return "normal";
        }
    }

    public BankUgyfel getUgyfel() {
        return ugyfel;
    }
}
