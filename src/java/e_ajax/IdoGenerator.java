/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package e_ajax;

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.faces.bean.ManagedBean;

@ManagedBean
public class IdoGenerator {

    private String formatum;
    private SimpleDateFormat sdf;
    private Date ido;

    public IdoGenerator() {
        formatum = "yyyy.MM.dd hh:mm:ss";
        sdf = new SimpleDateFormat(formatum);
    }

    public String getFormatum() {
        return formatum;
    }
    
    public void setFormatum(String formatum) {
        this.formatum = formatum;
    }

//    public void setFormatum(String formatum) {
//        this.formatum = formatum;
//        //this.sdf = new SimpleDateFormat(formatum);
//    }
    public String getIdo() {
        if (ido != null){
        sdf.applyPattern(formatum);
        return sdf.format(ido);
        } else  {
            return "";
        }
    }

    public String idotLeker() {
        ido = new Date();
        return (null);
    }
}
