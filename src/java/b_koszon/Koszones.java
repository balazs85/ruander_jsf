/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package b_koszon;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class Koszones {
    private String nev;

    public String getNev() {
        return nev;
    }

    public void setNev(String nev) {
        this.nev = nev.trim();
    }
    
    public String koszon(){
        if (nev == null || nev.trim().isEmpty()){
            return ("nincsKitoltve");
        } else {
            return("koszon");
        }
    }
}
