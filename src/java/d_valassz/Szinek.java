/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package d_valassz;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ManagedBean;

@ManagedBean
public class Szinek {
    private String[] szinNevek = {"red", "green", "blue"};
    private List<String> szinErtekek = Arrays.asList("#ff0000", "#00ff00", "#0000ff");
    private Map<String, String> szinMap = new LinkedHashMap<>();
    private List<Szin> szinObjektumok = new ArrayList<>();

    public Szinek() {
        for (int i = 0; i < szinNevek.length; i++) {
            szinMap.put(szinNevek[i], szinErtekek.get(i));
            szinObjektumok.add(new Szin(szinNevek[i], szinErtekek.get(i)));
        }
    }  
    
    public String[] getSzinNevek() {
        return szinNevek;
    }

    public List<String> getSzinErtekek() {
        return szinErtekek;
    }

    public Map<String, String> getSzinMap() {
        return szinMap;
    }

    public List<Szin> getSzinObjektumok() {
        return szinObjektumok;
    }
    
    
}
