/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package d_valassz;

/**
 *
 * @author nbalazs
 */
public class Szin {
    private String nev;
    private String ertek;

    public Szin(String nev, String ertek) {
        this.nev = nev;
        this.ertek = ertek;
    }

    public String getNev() {
        return nev;
    }

    public String getErtek() {
        return ertek;
    }
}
