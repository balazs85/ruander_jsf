/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package d_valassz;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class ValasztottSzin {
    private String szin1;
    private String szin2;
    private String szin3;
    private String szin4;

    public String getSzin1() {
        return szin1;
    }

    public void setSzin1(String szin1) {
        this.szin1 = szin1;
    }

    public String getSzin2() {
        return szin2;
    }

    public void setSzin2(String szin2) {
        this.szin2 = szin2;
    }

    public String getSzin3() {
        return szin3;
    }

    public void setSzin3(String szin3) {
        this.szin3 = szin3;
    }

    public String getSzin4() {
        return szin4;
    }

    public void setSzin4(String szin4) {
        this.szin4 = szin4;
    }
    
    public String szinMutatasa(){
        return("mutat");
    }
}
